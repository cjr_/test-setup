const assert = require('chai').assert;
const numberToString = require('../app.js');

describe('Number to String', () => {

  it('Converts a single digit', () => {
    assert.equal(numberToString(1), 'one')
  })

  it('Converts a single digit with a decimal to the hundreds place', () => {
    assert.equal(numberToString(1.2340), 'one and 23/100')
  })

  it('Converts a single digit and rounds the decimal to the hundreds place', () => {
    assert.equal(numberToString(1.2570), 'one and 26/100')
  })

  it('Converts a double digit', () => {
    assert.equal(numberToString(24), 'twenty four')
  })

  it('Converts a double digit with a decimal to the hundreds place', () => {
    assert.equal(numberToString(24.6573), 'one and 23/100')
  })

  // more tests here
});
